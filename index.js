//#2
console.log("hello world")


//#3 & #4
let number1 = parseInt(prompt("Enter first number"));
let number2 = parseInt(prompt("Enter second number"));

let total = number1 + number2;
let subtract = number1 - number2;
let multiply = number1 * number2;
let divide = number1 / number2;

if(total < 10) {
	console.log(total);
} else if(total >= 10 && total <= 20) {
	console.log(subtract)
} else if(total >= 21 && total <= 29) {
	console.log(multiply);
} else if (total >= 30) {
	console.log(divide);
}


//#5
let userName = prompt("Enter your name");
let age = parseInt(prompt("Enter your age"));

if(userName == "" || age == "") {
	alert("Are you a time traveler?");
} else {
	alert(`Hi ${userName} you are age ${age}`);
}

//#6
function isLegalAge(age) {
	if(age >= 18) {
		alert('You are of legal age');
	} else {
		alert('You are not allowed here');
	}
}

isLegalAge(age)


//#7
switch (age) {
	case 18:
		console.log('You are now allowed to party');
		break;
	case 21:
		console.log('You are now part of the adult society');
	case 65:
		console.log("We thank you for your contribution to the society");
	default:
		console.log("Are you sure you're not an alien?")
}

//#8 
try {
	isLegalAge(age);
} catch(error) {
	console.warn(error.message);
} finally {
	isLegalAge(age)
}





